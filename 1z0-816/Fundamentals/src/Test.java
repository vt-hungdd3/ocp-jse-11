import java.util.ArrayList;
import java.util.List;

abstract class A{
    protected int varTobeHidden  = 10;
    private int i ;
    private void m1(){}
    protected void m3(){ System.out.println("m3 A");}
    protected void chew(List<Double> input) {}


}
class B extends A{
    protected int varTobeHidden  = 20;
    //public  void m3(){System.out.println("m3 B");}
    void m2(){  m3(); }

    protected void chew(List<Double> input) {}
}

public class Test {
    public static void main(String... args){
        A ab = new B();
        System.out.println(ab.varTobeHidden);
        B b = new B();
        ab.m3();
        b.m2();
        System.out.println("DONE");

    }
}




