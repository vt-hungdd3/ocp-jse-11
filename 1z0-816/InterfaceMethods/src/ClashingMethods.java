/*
Center Of Excellence
  dangduyhung@gmail.com 

Java SE 11 Developer 1Z0-819 OCP Course - Part 2
Section 3: Interfaces
Topic:  Method clashing
*/

interface FirstInterface {
    // private method
    default void methodA() {
        System.out.println("FirstInterface's default method A");
    }
    static void methodB(){
        System.out.println("FirstInterface's static method B");

    }
}

interface SecondInterface {
    // static (and public) method
    default void methodA() {
        System.out.println("SecondInterface's default method A");
    }
}

public class ClashingMethods implements FirstInterface {
    public static void main(String[] args) {
        ClashingMethods first = new ClashingMethods();
        // Calling static method on an interface
        first.methodA();

    }
}