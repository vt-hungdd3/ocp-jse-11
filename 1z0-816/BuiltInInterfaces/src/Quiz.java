import java.util.function.Function;
import java.util.function.LongFunction;

public class Quiz {
    public  static void foo(Function<Integer,String> f){}
    public static void main(String... args){
//      foo(n->Integer.toHexString(n));
//      foo(Integer::toHexString);
//      foo(n::toHexString);
//      foo(n->Integer::toHexString);
//      foo(toHexString);
//      foo(int n -> Integer::toHexString(n));
//      foo((int n)->Integer::toHexString(n));
//      foo(n->n+1);

//----------------------------------------------------
        LongFunction f = x->x*x;
        System.out.println(f.apply(100));

//----------------------------------------------------
    }
}
